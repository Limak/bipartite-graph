#include "Queue.h"



void Queue::add(SetAVertex * value)
{

	if (first == NULL)
	{
		first = last = new Queue(value);
	}
	else
	{
		Queue* newElement = new Queue(value);
		if (first->next == nullptr)
			first->next = newElement;
		last->next = newElement;
		last = newElement;
	}

	// printf("%s %s\n", tmpLista.front(), tmpLista.back());

}


void Queue::removeElement()
{

	if (first == last)
	{
		first = NULL;
		last = NULL;
	}
	else
	{
		first = first->next;
	}
	
}

SetAVertex* Queue::getFirstElement()
{
	return first->value;
}


bool Queue::isEmpty()
{
	if (first == NULL)
		return true;
	return false;
}

Queue::Queue(SetAVertex* value) : value(value)
{
	this->first = NULL;
	this->last = NULL;
}

Queue::Queue()
{
	this->first = NULL;
	this->last = NULL;
}


Queue::~Queue()
{
}
