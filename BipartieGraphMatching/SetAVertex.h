﻿#pragma once
#include "SetBVertex.h"

class SetAVertex
{
public:
	
	SetAVertex()
	{
		matchedVertex = NO_MATCHED_VERTEX;
	}

	~SetAVertex()
	{
		delete[] neighbours;
	}

	void initNeighbours(int size)
	{
		neighboursSize = size;
		neighbours = new SetBVertex*[neighboursSize];
	}

	void insertNeighbour(int index, SetBVertex* neighbour)
	{
		neighbours[index] = neighbour;
	}

	void setDistance(int distance)
	{
		currentDistance = distance;
	}

	int getNeighboursSize()
	{
		return neighboursSize;
	}

	SetBVertex* getNeighbour(int index)
	{
		return neighbours[index];
	}

	void setMatchedVertex(SetBVertex* vertex)
	{
		matchedVertex = vertex;
	}

	SetBVertex* getMatchedVertex()
	{
		return matchedVertex;
	}

	int getDistance()
	{
		return currentDistance;
	}

	bool visited()
	{
		return wasVisited;
	}

	void setVisited(bool wasVisited)
	{
		this->wasVisited = wasVisited;
	}

	void setNeighbour(int index, SetBVertex* value)
	{
		neighbours[index] = value;
	}
private:

	int currentDistance;
	SetBVertex** neighbours;
	int neighboursSize;
	SetBVertex* matchedVertex;
	bool wasVisited;

};
