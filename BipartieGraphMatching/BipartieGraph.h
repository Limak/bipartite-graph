﻿#pragma once
#include "SetAVertex.h"
#include <queue>
#include "Queue.h"


class BipartieGraph
{
public:

	BipartieGraph(int setASize, int setBSize)
	{
		setAVerticles = new SetAVertex[setASize];
		setBVerticles = new SetBVertex[setBSize];
		this->setASize = setASize;
		this->setBSize = setBSize;
	}

	/*~BipartieGraph()
	{
		delete[] setAVerticles;
		delete[] setBVerticles;
	}*/


	void firstMatching()
	{
		for (int i = 0; i < setASize; i++)
		{
			SetAVertex* vertex = setAVerticles + i;
			int neighboursSize = vertex->getNeighboursSize();
			for (int j = neighboursSize - 1; j >= 0; j--)
			{
				SetBVertex* neighbour = vertex->getNeighbour(j);
				if(neighbour->getMatchedVertex() == NO_MATCHED_VERTEX)
				{
					neighbour->setMatchedVertex(vertex);
					vertex->setMatchedVertex(neighbour);
					break;
				}
			}
		}
	}

	void findBestMatching()
	{
		bfs();
		while (dfs())
		{
			bfs();
		}
	}

	int getNumberOfMatchedVerticles()
	{
		int matchedVerticles = 0;
		for (int i = 0; i < setASize; i++)
		{
			if (setAVerticles[i].getMatchedVertex() != NO_MATCHED_VERTEX)
				matchedVerticles++;
		}
		return matchedVerticles;
	}

	void initSetAVertexNeighbours(int i, int size)
	{
		setAVerticles[i].initNeighbours(size);
	}

	void setAVerticleNeighbour(int setA, int neighbourIndex, int neighbourId)
	{
		setAVerticles[setA].setNeighbour(neighbourIndex, setBVerticles + neighbourId - 1);
	}
private:

	void bfs()
	{
		Queue queue;
		for (int i = 0; i < setASize; i++)
		{
			SetAVertex* setAVertex = setAVerticles + i;
			setAVertex->setDistance(-1);
			if(setAVertex->getMatchedVertex() == NO_MATCHED_VERTEX)
			{
				setAVertex->setDistance(0);
				queue.add(setAVertex);
			}
		}

		while(!queue.isEmpty())
		{
			SetAVertex* setAVertex = queue.getFirstElement();
			queue.removeElement();
			int neighboursSize = setAVertex->getNeighboursSize();
			for (int i = 0; i < neighboursSize; i++)
			{
				SetBVertex* setBVertex = setAVertex->getNeighbour(i);
				SetAVertex* setBVertexNeighbour = setBVertex->getMatchedVertex();
				if(setBVertexNeighbour != NO_MATCHED_VERTEX && setBVertexNeighbour->getDistance() == -1)
				{
					setBVertexNeighbour->setDistance(setAVertex->getDistance() + 1);
					queue.add(setBVertexNeighbour);
				}
			}
		}
	}

	bool dfs()
	{
		for (int i = 0; i < setASize; i++)
		{
			setAVerticles[i].setVisited(false);
		}

		bool returnValue = false;
		for (int i = 0; i < setASize; i++)
		{
			if (setAVerticles[i].getMatchedVertex() == NO_MATCHED_VERTEX && dfsWithVertex(setAVerticles + i))
				returnValue = true;
		}
		return returnValue;
	}

	bool dfsWithVertex(SetAVertex* vertex)
	{
		vertex->setVisited(true);
		int neighboursSize = vertex->getNeighboursSize();
		for (int i = 0; i < neighboursSize; i++)
		{
			SetBVertex* setBVertex = vertex->getNeighbour(i);
			if(setBVertex->getMatchedVertex() == NO_MATCHED_VERTEX)
			{
				vertex->setMatchedVertex(setBVertex);
				setBVertex->setMatchedVertex(vertex);
				return true;
			}
			SetAVertex* setBVertexNeighbour = setBVertex->getMatchedVertex();

			if (!setBVertexNeighbour->visited() && 
				setBVertexNeighbour->getDistance() == vertex->getDistance() + 1 &&
				dfsWithVertex(setBVertexNeighbour))
			{
				vertex->setMatchedVertex(setBVertex);
				setBVertex->setMatchedVertex(vertex);
				return true;
			}
		}
		return false;			
	}


	SetAVertex* setAVerticles;
	int setASize;
	SetBVertex* setBVerticles;
	int setBSize;
};
