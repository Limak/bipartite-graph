#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include "BipartieGraph.h"

using namespace std;

int main()
{
	int setASize, setBSize;
	scanf("%d %d", &setASize, &setBSize);
	BipartieGraph graph(setASize, setBSize);
	for (int i = 0; i < setASize; i++)
	{
		int wantedNeighboursSize;
		scanf("%d", &wantedNeighboursSize);
		graph.initSetAVertexNeighbours(i, wantedNeighboursSize);
		int j = 0;
		while(j < wantedNeighboursSize)
		{
			int wantedNeighbourNumber;
			scanf("%d", &wantedNeighbourNumber);
			graph.setAVerticleNeighbour(i, j, wantedNeighbourNumber);
			j++;
		}
	}

	graph.findBestMatching();
	int result = graph.getNumberOfMatchedVerticles();
	printf("%d", result);
	return 0;
}