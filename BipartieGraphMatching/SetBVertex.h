﻿#pragma once
#include "BipartieGraph.h"

#define NO_MATCHED_VERTEX nullptr

class SetAVertex;

class SetBVertex
{
public:
	SetBVertex()
	{
		matchedVertex = NO_MATCHED_VERTEX;
	}


	SetAVertex* getMatchedVertex()
	{
		return matchedVertex;
	}

	void setMatchedVertex(SetAVertex* vertex)
	{
		matchedVertex = vertex;
	}

private:

	SetAVertex* matchedVertex;
};
