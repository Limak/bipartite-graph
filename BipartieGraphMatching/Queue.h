#pragma once
#include <cstdio>
#include <cstring>

class SetAVertex;

class Queue
{
public:
	Queue* first;
	Queue* last;
	SetAVertex* value;
	Queue* next;
	

	void add(SetAVertex * value);

	void removeElement();
	SetAVertex* getFirstElement();
	bool isEmpty();
	Queue(SetAVertex * value);
	Queue();
	~Queue();
};

